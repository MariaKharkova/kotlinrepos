package com.trendrepogithub.di

import android.app.Application
import com.trendrepogithub.ReposApp
import com.trendrepogithub.di.modules.*
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton


@Singleton
@Component(
    modules = [
        AndroidInjectionModule::class,
        NetworkModule::class, ViewModelsModule::class, ViewModelFactoryModule::class,
        DatabaseModule::class,
        BuildersModule::class,
        PreferencesModule::class]
)

interface AppComponent {
    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent
    }

    fun inject(application: ReposApp)
}