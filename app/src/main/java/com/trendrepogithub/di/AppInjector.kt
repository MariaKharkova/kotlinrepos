package com.trendrepogithub.di

import com.trendrepogithub.ReposApp

object AppInjector {
    fun init(application: ReposApp) {
        DaggerAppComponent.builder().application(application)
            .build().inject(application)
    }
}