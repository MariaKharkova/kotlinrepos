package com.trendrepogithub.di.modules

import androidx.lifecycle.ViewModel
import com.trendrepogithub.di.ViewModelKey
import com.trendrepogithub.presentation.details.RepoDetailsViewModel
import com.trendrepogithub.presentation.list.RepoListViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap


@Module
abstract class ViewModelsModule {
    @Binds
    @IntoMap
    @ViewModelKey(RepoListViewModel::class)
    abstract fun bindRepoListViewModel(repoListViewModel: RepoListViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(RepoDetailsViewModel::class)
    abstract fun bindRepoDetailsViewModel(detailsViewModel: RepoDetailsViewModel): ViewModel
}