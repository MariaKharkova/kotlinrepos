package com.trendrepogithub.di.modules

import androidx.lifecycle.ViewModelProvider
import com.trendrepogithub.presentation.ReposViewModelFactory
import dagger.Binds
import dagger.Module

@Module
abstract class ViewModelFactoryModule {
    @Binds
    abstract fun bindViewModelFactory(viewModelFactory: ReposViewModelFactory): ViewModelProvider.Factory
}