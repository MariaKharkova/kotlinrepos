package com.trendrepogithub.di.modules

import android.app.Application
import androidx.room.Room
import com.trendrepogithub.data.local.RepoDatabase
import com.trendrepogithub.data.local.dao.RepoDao
import dagger.Module
import dagger.Provides
import javax.inject.Singleton


@Module
class DatabaseModule {

    @Singleton
    @Provides
    fun providesRepoDatabase(application: Application): RepoDatabase {
        return Room.databaseBuilder(application, RepoDatabase::class.java, "repo-github-db").build()
    }

    @Singleton
    @Provides
    fun providesRepoDao(repoDatabase: RepoDatabase): RepoDao {
        return repoDatabase.repoDao()
    }
}