package com.trendrepogithub.di.modules

import com.trendrepogithub.presentation.details.RepoDetailsFragment
import com.trendrepogithub.presentation.list.RepoListFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class BuildersModule {

    @ContributesAndroidInjector
    abstract fun contributeRepoListFragment(): RepoListFragment

    @ContributesAndroidInjector
    abstract fun contributeRepoDetailsFragment(): RepoDetailsFragment
}
