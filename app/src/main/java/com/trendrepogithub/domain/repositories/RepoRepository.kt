package com.trendrepogithub.domain.repositories

import android.annotation.SuppressLint
import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import androidx.paging.PagedList
import androidx.paging.RxPagedListBuilder
import com.trendrepogithub.data.api.GithubApiInterface
import com.trendrepogithub.data.local.dao.RepoDao
import com.trendrepogithub.data.local.preferences.AppPreferences
import com.trendrepogithub.domain.models.entity.Repo
import com.trendrepogithub.domain.models.wrappers.Resource
import com.trendrepogithub.domain.repositories.helper.mapToReposList
import io.reactivex.BackpressureStrategy
import io.reactivex.Flowable
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import retrofit2.HttpException
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class RepoRepository
@Inject
constructor(
    private val githubApi: GithubApiInterface,
    private val repoDao: RepoDao,
    private val appPreferences: AppPreferences
) {

    private var page: Int = appPreferences.getCurrentPage()

    private var networkStatus: MutableLiveData<Resource<String>> = MutableLiveData()

    fun getNetworkStatus() = networkStatus

    fun getRepoDetails(id: Long):Single<Repo>{
        return repoDao.getRepositoryById(id)
    }

    fun getRepoPagedList(): Flowable<PagedList<Repo>> {
        return RxPagedListBuilder(
            getRepoDataSource(),
            PagedList.Config
                .Builder()
                .setPageSize(100)
                .setPrefetchDistance(1)
                .setEnablePlaceholders(false)
                .build())
            .setBoundaryCallback(
                object : PagedList.BoundaryCallback<Repo>() {
                    override fun onZeroItemsLoaded() {
                        super.onZeroItemsLoaded()
                        loadAndSaveRepos(1)
                    }

                    override fun onItemAtEndLoaded(itemAtEnd: Repo) {
                        super.onItemAtEndLoaded(itemAtEnd)
                        page +=1
                        loadAndSaveRepos(page) }
                })
            .buildFlowable(BackpressureStrategy.LATEST)
    }

    @SuppressLint("CheckResult")
    private fun loadAndSaveRepos(page: Int) {
        loadReposFromNetwork(page)
            .subscribeOn(Schedulers.io())
            .subscribe (
                {
                    appPreferences.setCurrentPage(page)
                    networkStatus.postValue(Resource.success(null))
                    repoDao.insertRepos(it)
                },{
                    if (it !is HttpException || it.code()!=403) {
                        networkStatus.postValue(Resource.error(it.message))
                    }else {
                        networkStatus.postValue(Resource.success(null))
                    }
                }
            )
    }

    private fun getRepoDataSource(): DataSource.Factory<Int, Repo> {
        return repoDao.selectPaged()
    }

    private fun loadReposFromNetwork(page: Int): Single<List<Repo>> {
        return githubApi.getRepos(page = page).map {
            return@map mapToReposList(it)
        }
    }
}