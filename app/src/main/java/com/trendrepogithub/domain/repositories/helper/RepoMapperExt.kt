package com.trendrepogithub.domain.repositories.helper

import com.trendrepogithub.data.api.response.ReposResponse
import com.trendrepogithub.domain.models.entity.Repo

fun mapToReposList(response: ReposResponse): List<Repo> {
    val repos: MutableList<Repo> = mutableListOf()
    for (item in response.reposList) {
        repos.add(Repo(item))
    }
    return repos.toList()
}