package com.trendrepogithub.domain.models.entity

import androidx.annotation.NonNull
import androidx.recyclerview.widget.DiffUtil
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.squareup.moshi.Json
import com.trendrepogithub.data.api.response.Repository

@Entity(tableName = "repo")
data class Repo(
    @PrimaryKey
    val id: Long,
    val name: String,
    val fullName: String?,
    val description: String?,
    val language: String?,
    val score: Double,
    @Json(name = "html_url")
    val url: String?,
    val stargazersCount: Int,
    val watchersCount: Int,
    val forksCount: Int,
    val openIssuesCount: Int
) {
    constructor (repositoryItem: Repository) : this(
        id = repositoryItem.id,
        name = repositoryItem.name,
        fullName = repositoryItem.fullName,
        description = repositoryItem.description,
        language = repositoryItem.language,
        url = repositoryItem.url,
        score = repositoryItem.score,
        stargazersCount = repositoryItem.stargazersCount,
        watchersCount = repositoryItem.watchersCount,
        forksCount = repositoryItem.forksCount,
        openIssuesCount = repositoryItem.openIssuesCount
    )

    companion object {
        var DIFF_CALLBACK: DiffUtil.ItemCallback<Repo> = object : DiffUtil.ItemCallback<Repo>() {
            override fun areItemsTheSame(@NonNull oldItem: Repo, @NonNull newItem: Repo): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(@NonNull oldItem: Repo, @NonNull newItem: Repo): Boolean {
                return oldItem == newItem
            }
        }
    }
}