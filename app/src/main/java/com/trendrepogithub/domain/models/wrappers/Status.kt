package com.trendrepogithub.domain.models.wrappers

enum class Status {
    SUCCESS,
    ERROR,
    LOADING;
}