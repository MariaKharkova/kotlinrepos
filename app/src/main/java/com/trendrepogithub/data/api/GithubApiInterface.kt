package com.trendrepogithub.data.api

import com.trendrepogithub.data.api.response.ReposResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query


interface GithubApiInterface {

    @GET("search/repositories")
    fun getRepos(
        @Query("q") topic: String = "kotlin+language:kotlin",
        @Query("sort") sort: String = "stars",
        @Query("order") order: String = "desc",
        @Query("page") page: Int
    ): Single<ReposResponse>
}