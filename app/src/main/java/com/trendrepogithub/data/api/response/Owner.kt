package com.trendrepogithub.data.api.response

import com.squareup.moshi.Json

data class Owner(
    val id: Long,
    @Json(name = "node_id")
    val nodeId: String,
    val login: String,
    @Json(name = "avatar_url")
    val avatarUrl: String?,
    @Json(name = "received_events_url")
    val receivedEventsUrl: String?,
    val type: String

)