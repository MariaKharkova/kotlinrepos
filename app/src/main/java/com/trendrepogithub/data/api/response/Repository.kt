package com.trendrepogithub.data.api.response

import com.squareup.moshi.Json

data class Repository (
    val id: Long,
    @Json(name = "node_id")
    val nodeId: String,
    val name: String,
    @Json(name = "full_name")
    val fullName: String?,
    val owner: Owner,
    val private: Boolean,
    val fork: Boolean,
    val description: String?,
    val language: String?,
    val url: String?,
    val homepage: String?,
    @Json(name = "html_url")
    val htmlUrl: String?,
    @Json(name = "created_at")
    val createdAt: String?,
    @Json(name = "updated_at")
    val updatedAt: String?,
    @Json(name = "pushed_at")
    val pushedAt: String?,
    val size: Int,
    val score: Double,
    @Json(name = "stargazers_count")
    val stargazersCount: Int,
    @Json(name = "watchers_count")
    val watchersCount: Int,
    @Json(name = "forks_count")
    val forksCount: Int,
    @Json(name = "open_issues_count")
    val openIssuesCount: Int,
    @Json(name = "master_branch")
    val masterBranch: String?,
    @Json(name = "default_branch")
    val defaultBranch: String?
)