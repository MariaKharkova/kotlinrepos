package com.trendrepogithub.data.api.response

import com.squareup.moshi.Json

data class ReposResponse(
    @Json(name = "total_count")
    val total_count: Int,
    @Json(name = "incomplete_results")
    val hasIncompleteResults: Boolean,
    @Json(name = "items")
    val reposList: List<Repository>
)