package com.trendrepogithub.data.local.dao

import androidx.paging.DataSource
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.trendrepogithub.domain.models.entity.Repo
import io.reactivex.Observable
import io.reactivex.Single

@Dao
abstract class RepoDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insert(vararg repos: Repo)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertRepos(repositories: List<Repo>)

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    abstract fun createRepoIfNotExists(repo: Repo): Long

    @Query("SELECT * FROM repo ORDER BY stargazersCount DESC")
    abstract fun loadRepositories(): Observable<List<Repo>>

    @Query("SELECT * FROM repo WHERE id = :repoId")
    abstract fun getRepositoryById(repoId: Long): Single<Repo>

    @Query("SELECT * FROM repo ORDER BY stargazersCount DESC")
    abstract fun selectPaged(): DataSource.Factory<Int, Repo>
}
