package com.trendrepogithub.data.local.preferences

import android.content.SharedPreferences
import javax.inject.Inject

private const val PREFERENCES_CURRENT_PAGE = "com.trendrepogithub.data.PREFERENCES_CURRENT_PAGE"

class AppPreferences @Inject constructor(private val preferences: SharedPreferences) {

    fun setCurrentPage(newCurrentPage: Int) {
        preferences.edit().putInt(PREFERENCES_CURRENT_PAGE, newCurrentPage).apply()
    }

    fun getCurrentPage(): Int {
        return (preferences.getInt(PREFERENCES_CURRENT_PAGE, 0))
    }
}