package com.trendrepogithub.data.local

import androidx.room.Database
import androidx.room.RoomDatabase
import com.trendrepogithub.data.local.dao.RepoDao
import com.trendrepogithub.domain.models.entity.Repo

@Database(
    entities = [
        Repo::class],
    version = 1
)
abstract class RepoDatabase : RoomDatabase() {

    abstract fun repoDao(): RepoDao
}