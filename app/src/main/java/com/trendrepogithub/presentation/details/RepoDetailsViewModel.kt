package com.trendrepogithub.presentation.details

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.trendrepogithub.domain.models.entity.Repo
import com.trendrepogithub.domain.models.wrappers.Resource
import com.trendrepogithub.domain.repositories.RepoRepository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class RepoDetailsViewModel @Inject constructor(private val repoRepository: RepoRepository) : ViewModel() {
    private var disposable: CompositeDisposable? = CompositeDisposable()
    private val repoDetails = MutableLiveData<Resource<Repo>>()
    private var repoId: Long = -1L


    fun getRepoDetails(id: Long): MutableLiveData<Resource<Repo>> {
        repoId = id
        repoDetails.value = Resource.loading()
        disposable?.add(
            repoRepository.getRepoDetails(repoId).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { repoDetails.value = Resource.loading() }
                .subscribe { details, error ->
                    if (error!=null){
                        repoDetails.value = Resource.error(error.message)
                    }else{
                        repoDetails.value = Resource.success(details)
                    }
                }
        )
        return repoDetails
    }

    override fun onCleared() {
        super.onCleared()
        disposable?.clear()
        disposable = null
    }
}