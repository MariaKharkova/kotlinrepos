package com.trendrepogithub.presentation.details


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.trendrepogithub.databinding.RepoDetailsLayoutBinding
import com.trendrepogithub.domain.models.entity.Repo
import com.trendrepogithub.domain.models.wrappers.Resource
import com.trendrepogithub.domain.models.wrappers.Status
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.repo_details_layout.*
import javax.inject.Inject


class RepoDetailsFragment : Fragment() {

    private lateinit var binding: RepoDetailsLayoutBinding
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var viewModel: RepoDetailsViewModel
    private val repoObserver = Observer<Resource<Repo>> { response ->
        response?.let {
            when (it.status) {
                Status.SUCCESS -> {
                    binding.repo = it.data
                    binding.executePendingBindings()
                    detailsProgress.visibility = View.GONE
                }
                Status.ERROR -> {
                    detailsProgress.visibility = View.GONE
                    Toast.makeText(context, it.errorMessage, Toast.LENGTH_SHORT).show()
                }
                Status.LOADING -> detailsProgress.visibility = View.VISIBLE
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, com.trendrepogithub.R.layout.repo_details_layout, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        AndroidSupportInjection.inject(this)
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(RepoDetailsViewModel::class.java)
        arguments?.let {
            viewModel.getRepoDetails(RepoDetailsFragmentArgs.fromBundle(it).repoId).observe(this, repoObserver)
        }
    }


}