package com.trendrepogithub.presentation.list

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.paging.PagedList
import com.trendrepogithub.domain.models.entity.Repo
import com.trendrepogithub.domain.models.wrappers.Resource
import com.trendrepogithub.domain.repositories.RepoRepository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class RepoListViewModel @Inject constructor(private val reposRepository: RepoRepository) : ViewModel() {
    private var disposable: CompositeDisposable? = CompositeDisposable()
    private val reposList = MutableLiveData<Resource<PagedList<Repo>>>()
    private val networkStatus = reposRepository.getNetworkStatus()
    private var isLoading = false

    init {
        getRepositories()
    }

    private fun getRepositories() {
        if (!isLoading) {
            isLoading = true
            reposList.value = Resource.loading()
            disposable?.add(
                reposRepository.getRepoPagedList().subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe ({
                        reposList.value = Resource.success(it)
                        isLoading = false
                    },{
                    reposList.value = Resource.error(it.message)
                })
            )
        }
    }

    override fun onCleared() {
        super.onCleared()
        disposable?.clear()
        disposable = null
    }
    internal fun getReposList(): MutableLiveData<Resource<PagedList<Repo>>> {
        return reposList
    }

    internal fun getNetworkStatus() = networkStatus
}
