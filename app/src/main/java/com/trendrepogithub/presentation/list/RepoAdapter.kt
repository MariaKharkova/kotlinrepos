package com.trendrepogithub.presentation.list

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.NonNull
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.trendrepogithub.databinding.ItemRepoLayoutBinding
import com.trendrepogithub.domain.models.entity.Repo

class RepoAdapter internal constructor(private val context: Context, private val listener: RepoActionListener) :
    PagedListAdapter<Repo, RepoAdapter.RepoViewHolder>(Repo.DIFF_CALLBACK) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RepoViewHolder {
        return RepoViewHolder(ItemRepoLayoutBinding.inflate(LayoutInflater.from(context), parent, false))
    }

    override fun onBindViewHolder(@NonNull holder: RepoViewHolder, position: Int) {
        holder.bind(getItem(position)!!)
    }

    override fun getItemId(position: Int): Long {
        return super.getItemId(position)
    }

    inner class RepoViewHolder internal constructor(private val binding: ItemRepoLayoutBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(repository: Repo) {
            itemView.setOnClickListener { listener.onOpenRepoDetails(repository.id) }
            binding.repo = repository
            binding.executePendingBindings()
        }
    }
}

interface RepoActionListener {
    fun onOpenRepoDetails(repoId: Long)
}