package com.trendrepogithub.presentation.list

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import androidx.paging.PagedList
import com.trendrepogithub.R
import com.trendrepogithub.domain.models.entity.Repo
import com.trendrepogithub.domain.models.wrappers.Resource
import com.trendrepogithub.domain.models.wrappers.Status
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.repos_list_fragment.*
import javax.inject.Inject


class RepoListFragment : Fragment(){

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var viewModel: RepoListViewModel
    private lateinit var adapter: RepoAdapter
    private val networkStatusObserver = Observer<Resource<String>> { response ->
        response?.let {
                if (it.status == Status.ERROR){
                    Log.e("fordidden", it.errorMessage)
                    Toast.makeText(context, it.errorMessage, Toast.LENGTH_SHORT).show()
                }
        }
    }
    private val reposListObserver = Observer<Resource<PagedList<Repo>>> { response ->
        response?.let {
            when (it.status) {
                Status.SUCCESS -> {
                    adapter.submitList(it.data)
                }
                Status.ERROR -> {
                    Log.e("fordidden", it.errorMessage)
                    Toast.makeText(context, it.errorMessage, Toast.LENGTH_SHORT).show()
                }
                Status.LOADING ->  /* adapter.showLoading()*/ Log.d("gg","ff")
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.repos_list_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        AndroidSupportInjection.inject(this)
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(RepoListViewModel::class.java)
        setUpReposList()
        viewModel.getReposList().observe(viewLifecycleOwner, reposListObserver)
        viewModel.getNetworkStatus().observe(viewLifecycleOwner, networkStatusObserver)
    }

    private fun setUpReposList(){
        adapter = RepoAdapter(
            context = context!!,
            listener = object : RepoActionListener {
                override fun onOpenRepoDetails(repoId: Long) {
                    findNavController().navigate(RepoListFragmentDirections.actionToDetails().setRepoId(repoId))
                }
            })
        reposList.adapter = adapter
}



}
